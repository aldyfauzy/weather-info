package com.example.weatherinfo.util;

import android.Manifest;

/**
 * Created by Aldy on 7/31/2019.
 */
public class Constant {
    public static final String APPID = "fdf871cedaf3413c6a23230372c30a02";
    public static final String LAT = "latitude";
    public static final String LON = "longitude";

    public static final int PERMISSION_CODE = 1;
    public static final String[] PERMISSIONS_LIST = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
}
