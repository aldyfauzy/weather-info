package com.example.weatherinfo.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.renderscript.Sampler;
import android.support.v4.app.ActivityCompat;

import java.text.DecimalFormat;

/**
 * Created by Aldy on 7/31/2019.
 */
public class Util {
    public static String convertKelvinCelsius(double kelvin){
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        String temmp = String.valueOf(kelvin).replace(",",".");
        double result = 0;
        result = Double.valueOf(temmp) - 273.15;
        return decimalFormat.format(result);
    }

    public static String convertKelvinFahrenheit(double kelvin){
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        String temmp = String.valueOf(kelvin).replace(",",".");
        double result = 0;
        result = (Double.valueOf(temmp) - 273.15) * 9/5 + 32;
        return decimalFormat.format(result);
    }

    public static void requestPermission(Activity activity){
        ActivityCompat.requestPermissions(activity, Constant.PERMISSIONS_LIST, Constant.PERMISSION_CODE);
    }

    public static boolean checkPermission(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                context != null &&
                Constant.PERMISSIONS_LIST != null) {

            for (String permission : Constant.PERMISSIONS_LIST) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
