package com.example.weatherinfo.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Aldy on 7/31/2019.
 */
public class WeatherData {
    @SerializedName("coord")
    @Expose
    public Coordinate coord;

    @SerializedName("weather")
    @Expose
    public List<Weather> weather = null;

    @SerializedName("base")
    @Expose
    public String base;

    @SerializedName("main")
    @Expose
    public Temperature temperature;

    @SerializedName("wind")
    @Expose
    public Wind wind;

    @SerializedName("dt")
    @Expose
    public Integer dt;

    @SerializedName("timezone")
    @Expose
    public Integer timezone;

    @SerializedName("id")
    @Expose
    public Integer id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("cod")
    @Expose
    public Integer cod;
}
