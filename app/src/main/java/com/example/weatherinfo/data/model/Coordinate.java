package com.example.weatherinfo.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aldy on 7/31/2019.
 */
public class Coordinate {
    @SerializedName("lon")
    @Expose
    public double lon;

    @SerializedName("lat")
    @Expose
    public double lat;
}
