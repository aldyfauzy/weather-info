package com.example.weatherinfo.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Aldy on 7/31/2019.
 */
public class Temperature {
    @SerializedName("temp")
    @Expose
    public Double temp;

    @SerializedName("pressure")
    @Expose
    public Double pressure;

    @SerializedName("humidity")
    @Expose
    public Integer humidity;

    @SerializedName("temp_min")
    @Expose
    public Double tempMin;

    @SerializedName("temp_max")
    @Expose
    public Double tempMax;
}
