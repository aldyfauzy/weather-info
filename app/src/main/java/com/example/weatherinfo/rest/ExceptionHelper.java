package com.example.weatherinfo.rest;

/**
 * Created by Aldy on 7/31/2019.
 */
public class ExceptionHelper {

    private static final String ERROR_MESSAGE_DEFAULT = "Something went wrong";

    public static String getMessage(Throwable exception) {

        Throwable cause = exception.getCause();
        if (cause == null) {
            return ERROR_MESSAGE_DEFAULT;
        }

        return cause.getMessage();
    }
}

