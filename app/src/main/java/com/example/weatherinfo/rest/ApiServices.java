package com.example.weatherinfo.rest;

import com.example.weatherinfo.data.model.WeatherData;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Aldy on 7/31/2019.
 */
public interface ApiServices {

    String LAT = "lat";
    String LON = "lon";
    String APPID = "APPID";

    //Url paths
    String CURRENT_WEATHER = "weather";

    @GET(CURRENT_WEATHER)
    Observable<WeatherData> getCurrentWeather(@Query(LAT) Double lat,
                                              @Query(LON) Double lon,
                                              @Query(APPID) String appId);
}
