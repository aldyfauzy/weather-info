package com.example.weatherinfo.module;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.weatherinfo.BuildConfig;
import com.example.weatherinfo.R;
import com.example.weatherinfo.data.model.Weather;
import com.example.weatherinfo.data.model.WeatherData;
import com.example.weatherinfo.rest.ApiClient;
import com.example.weatherinfo.rest.ApiServices;
import com.example.weatherinfo.rest.ExceptionHelper;
import com.example.weatherinfo.util.Constant;
import com.example.weatherinfo.util.Util;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.txt_location)
    TextView txtLocation;

    @BindView(R.id.txt_lat)
    TextView txtLat;

    @BindView(R.id.txt_lon)
    TextView txtLon;

    @BindView(R.id.txt_temp)
    TextView txtTemp;

    @BindView(R.id.txt_c_f)
    TextView txtTempStat;

    @BindView(R.id.txt_main_temp)
    TextView txtMainTemp;

    @BindView(R.id.txt_desc_temp)
    TextView txtDescTemp;

    @BindView(R.id.txt_hum)
    TextView txtHumidity;

    @BindView(R.id.txt_wind)
    TextView txtWindSpeed;

    @BindView(R.id.img_temp)
    ImageView imgTemp;

    @BindView(R.id.switch_compat)
    SwitchCompat changeTemp;

    @BindView(R.id.btn_map)
    Button btnMaps;

    private double lat;
    private double lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if(!Util.checkPermission(this)){
            Util.requestPermission(this);
        } else {
            getCurrentLoc();
        }
    }

    @SuppressLint("MissingPermission")
    private void getCurrentLoc(){
        //get last location
        FusedLocationProviderClient mFusedLocation = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocation.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null){

                    //Set text location
                    lat = location.getLatitude();
                    lon = location.getLongitude();

                    // Display in Toast
                    Toast.makeText(MainActivity.this,
                            "Lat : " + location.getLatitude() + " Long : " + location.getLongitude(),
                            Toast.LENGTH_LONG).show();

                    //get weather data
                    getCurrentWeather(location.getLatitude(), location.getLongitude());
                } else {
                    Toast.makeText(MainActivity.this,
                            "please check your GPS and play service",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void initData(WeatherData data){

        //card top layout
        txtLat.setText("Lat : " + lat);
        txtLon.setText("Lon : " + lon);
        txtLocation.setText(data.name+"");
        txtTemp.setText(Util.convertKelvinCelsius(data.temperature.temp)+"");
        txtMainTemp.setText(data.weather.get(0).main +"");
        txtDescTemp.setText(data.weather.get(0).description +"");
        Glide.with(getApplicationContext())
                .load(BuildConfig.BASE_URL_IMG + data.weather.get(0).icon +".png")
                .fitCenter()
                .into(imgTemp);

        //card bottom layout
        txtHumidity.setText(data.temperature.humidity +" %");
        txtWindSpeed.setText(data.wind.speed + " m/s");

        //convert temp
        changeTemp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    txtTemp.setText(Util.convertKelvinFahrenheit(data.temperature.temp)+"");
                    txtTempStat.setText("\u2109");
                } else {
                    txtTemp.setText(Util.convertKelvinCelsius(data.temperature.temp)+"");
                    txtTempStat.setText("\u2103");
                }
            }
        });

        //btn go to map
        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                intent.putExtra(Constant.LAT, String.valueOf(lat));
                intent.putExtra(Constant.LON, String.valueOf(lon));
                startActivity(intent);
            }
        });
    }

    //----RX----//
    public void getCurrentWeather(Double lat, Double lon){
        Observable<WeatherData> discoverObservable = ApiClient.getRxInstance(this)
                .create(ApiServices.class)
                .getCurrentWeather(lat, lon, Constant.APPID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread());

        discoverObservable.subscribe(new Observer<WeatherData>() {
            @Override
            public void onSubscribe(Disposable d) { }

            @Override
            public void onNext(WeatherData resp) {
                if(resp != null){
                    initData(resp);
                }
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(MainActivity.this,
                        ExceptionHelper.getMessage(e),
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onComplete() { }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        txtTempStat.setText("\u2103");
        changeTemp.setChecked(false);
        if(Util.checkPermission(this)){
            getCurrentLoc();
        }
    }
}
