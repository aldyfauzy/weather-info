package com.example.weatherinfo.module;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.example.weatherinfo.R;
import com.example.weatherinfo.data.model.WeatherData;
import com.example.weatherinfo.rest.ApiClient;
import com.example.weatherinfo.rest.ApiServices;
import com.example.weatherinfo.rest.ExceptionHelper;
import com.example.weatherinfo.util.Constant;
import com.example.weatherinfo.util.Util;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

/**
 * Created by Aldy on 7/31/2019.
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double lat = 0;
    private double lon = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //prepare data
        if (getIntent().getStringExtra(Constant.LAT) != null
                && getIntent().getStringExtra(Constant.LON) != null) {
            lat = Double.parseDouble(getIntent().getStringExtra(Constant.LAT));
            lon = Double.parseDouble(getIntent().getStringExtra(Constant.LON));
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initMarker(WeatherData data){
        LatLng location = new LatLng(data.coord.lat, data.coord.lon);
        String uri = "@drawable/w" + data.weather.get(0).icon;
        int imageResource = getResources().getIdentifier(uri, null, getPackageName());

        // Setting marker
        Marker marker;
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(location);
        markerOptions.title(data.name);
        markerOptions.snippet(data.weather.get(0).main + ", " + Util.convertKelvinCelsius(data.temperature.temp) + " \u2103, "
                + data.temperature.humidity + " %, " + data.wind.speed + " m/s");
        markerOptions.icon(BitmapDescriptorFactory.fromResource(imageResource));

        mMap.clear();
        marker =  mMap.addMarker(markerOptions);
        marker.showInfoWindow();

        // Display in Toast
        Toast.makeText(MapsActivity.this,
                "Lat : " + data.coord.lat + " Long :" + data.coord.lon,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng location = new LatLng(lat, lon);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 8.0f));

        //add marker when map clicked
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                getCurrentWeather(point.latitude, point.longitude);
            }
        });
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    //----RX----//
    public void getCurrentWeather(Double lat, Double lon){
        Observable<WeatherData> discoverObservable = ApiClient.getRxInstance(this)
                .create(ApiServices.class)
                .getCurrentWeather(lat, lon, Constant.APPID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread());

        discoverObservable.subscribe(new Observer<WeatherData>() {
            @Override
            public void onSubscribe(Disposable d) { }

            @Override
            public void onNext(WeatherData resp) {
                if(resp != null){
                    initMarker(resp);
                }
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(MapsActivity.this,
                        ExceptionHelper.getMessage(e),
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onComplete() { }
        });
    }
}
